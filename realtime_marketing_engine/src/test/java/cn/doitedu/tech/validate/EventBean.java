package cn.doitedu.tech.validate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public  class EventBean{
    int userId;
    String eventId;
    long eventTime;
}
