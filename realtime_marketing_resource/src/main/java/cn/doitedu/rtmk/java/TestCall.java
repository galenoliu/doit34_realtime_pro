package cn.doitedu.rtmk.java;

import groovy.lang.GroovyClassLoader;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class TestCall {

    public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException {

        String code = FileUtils.readFileToString(new File("d:/code.txt"), "utf-8");

        GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
        Class aClass = groovyClassLoader.parseClass(code);

        Operator o = (Operator) aClass.newInstance();

        System.out.println(o.add(5, 6));
    }
}
